#!/bin/bash

NEWLOC=`curl -I "https://desktop.docker.com/mac/main/amd64/Docker.dmg" 2>/dev/null | grep etag:`

NEWLOC_ARM=`curl -I "https://desktop.docker.com/mac/main/arm64/Docker.dmg" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | grep etag:`


if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
